# Purpose: To predict substrate for RCAs
# Summary: - Use all observations within the coastal raster stack to model substrate
#          - Predict substrate only to RCAs
# Author: Jessica Nephin
# Some code snippets sourced from Cole Fields and Ed Gregr's version 
# (https://gitlab.com/dfo-msea/environmental-layers/random-forest-substrate)


# Required packages
library(caret)
library(ranger)
library(dplyr)
library(terra)
library(sf)



# Move back to parent directory
# Note: never run this twice in one session
setwd('..')



####################
##### Options  #####
####################

# Random forest options
RFC <- list( 'ntree'=1000,
             'repl'=TRUE,
             'imp'='impurity',
             'test.frac'=0.6 )



#################
####  Paths  ####
#################

# # Geodatabase with substrate observations as BType4 field
# inObs <- "bottom_grab_observations.gdb"
# Folder within working directory with raster predictor layers
inPreds <- "G:/Bathymetry/10m/NRCan"
# Current speed layer
currLoc <- 'G:/Ocean-Models/Coastwide_CurrentSpeed/Coastwide_Interpolation/Coastwide_CurrentSpeedIndex_10m_Masked.tif'

# Output directory
outRes <- "Results"
#dir.create(outRes)




########################
#####  Load Inputs  ####
########################

# Make a list of predictor rasters and stack
raster.list <- list.files(path = inPreds, pattern = '.tif$', full.names = TRUE)
preds.list <- raster.list[c(4,3,6,8,11)]
preds.list
predstack <- rast(c(preds.list, currLoc))
names(predstack) <- c('Depth','BBPI', "FBPI", "Slope", "SD", "Current")


# Read the substrate obs feature
substrateobs  <- read_sf(dsn = inObs,layer = "BC_BType_All")
# Convert to hard/soft
substrateobs$BType4[substrateobs$BType4 == 2] <- 1
substrateobs$BType4[substrateobs$BType4 == 3] <- 2
substrateobs$BType4[substrateobs$BType4 == 4] <- 2


########################
#####  Prep Inputs  ####
########################

# Remove NRCan observations because of truncated lat/lon positions
a <- nrow(substrateobs)
substrateobs <- substrateobs[substrateobs$BT_Source != "NRCan_Observations", ]
b <- nrow(substrateobs)
# Percent of obs dropped
(1 - (b/a)) * 100
# Number of obs by source
table(substrateobs$BT_Source)

# Remove BType other than 1, 2, 3, 4
a <- nrow(substrateobs)
substrateobs <- substrateobs[substrateobs$BType4 %in% 1:4, ]
b <- nrow(substrateobs)
# Percent of obs dropped
(1 - (b/a)) * 100
table(substrateobs$BType4) # 1 = hard, 2 = soft

# Extract predictor values at location of obs
preds <- extract(predstack, substrateobs, df = TRUE)

# Combine obs and preds into single dataframe
dat <- data.frame(obs=substrateobs[["BType4"]], preds[,-1])

# Remove records with NA values (can occur when obs are outside of preds)
a <- nrow(dat)
dat <- dat %>% drop_na() %>% as.data.frame()
b <- nrow(dat)
# Percent of obs dropped
(1 - (b/a)) * 100

# Set obs as factor
dat$obs <- factor(dat$obs)

# Save predictor dataset
save(dat, file=file.path(outRes, "Substrate_Obs_Preds_Dataset.RData"))
#load(file.path(outRes, "Substrate_Obs_Preds_Dataset.RData"))



########################
#####   Fit Model   ####
########################

# Randomly sample rows to create train and test indices
# Train data = 95%, test data = 5%
ind <- 1:nrow(dat)
train <- sample(ind, size=length(ind)*.95)
test <- ind[!(ind %in% train)]


# Model formula
mod.formula <- "obs ~ Depth + Slope + Current + BBPI + FBPI + SD"

# Calculate weights based on prevalence of each class
counts <- c(table(dat[train, "obs"]))
wts <- 1 - ( counts / sum( counts ))

# Fit model
sub.model <- ranger( formula = mod.formula,
                     data = dat[train,],
                     num.trees = RFC$ntree,
                     replace = RFC$repl,
                     importance = RFC$imp,
                     oob.error = T,
                     case.weights = wts[ dat[train, "obs"] ] )
# Check
sub.model

# Save
save(sub.model, file = file.path(outRes, "Substrate_Model.RData") )
#load(file.path(outRes, "Substrate_Model.RData"))



#######################
#####   Evaluate   ####
#######################

trainpred <- sub.model$predictions
testpred <- predict( sub.model, dat[test, ] )$predictions

# Generate a confusion matrix and evaluation statistics
train.stats <- caret::confusionMatrix( dat[train, "obs"],trainpred )
test.stats <- caret::confusionMatrix( dat[test, "obs"], testpred )

# Calculate TSS to summary
train.stats$byClass <- c(train.stats$byClass,
                         TSS=(train.stats$byClass[1] +
                                train.stats$byClass[2] - 1))
test.stats$byClass <- c(test.stats$byClass,
                        TSS=(test.stats$byClass[1] +
                               test.stats$byClass[2] - 1))

# Check stats
train.stats
test.stats


# Write as text file
sink(file = file.path(outRes, 'EvaluationStats.txt'), append = FALSE)
cat('***********************************************\n')
cat('Train data:\n\n')
print(train.stats)
cat('\n\n\n***********************************************\n')
cat('Test data:\n\n')
print(test.stats)
sink()




######################
#####   Predict   ####
######################

# Template raster
r <- predstack[[1]]
rrows <- nrow(r)
rcols <- ncol(r)

# Predict, one block at a time
nblocks <- 210
start_block <- 1
blocksize <- 450
iter <- data.frame(block=start_block:nblocks, start=NA, end=NA)
for (b in start_block:nblocks){
  # Get indices
  if(b == start_block){
    start <- 1
  } else {
    start <- end + 1
  }
  if(b == nblocks){
    end <- rrows
  } else {
    end <- start + blocksize
  }
  # fill dataframe
  iter$start[iter$block == b] <- start
  iter$end[iter$block == b] <- end
}

predBlocks <- function(x){
  # Get block, start and end
  block <- x['block']
  start <- x['start']
  end <- x['end']
  # create df for predicting
  cellnums <- terra::cellFromRowColCombine(predstack, row=start:end, col=1:rcols)
  vals_block <- terra::extract(x=predstack, y=cellnums)
  vals_block$cell <- cellnums
  vals_block <- vals_block %>% filter(!is.na(Current))
  vals_block <- vals_block %>% filter(!is.na(SD))
  vals_block <- vals_block %>% filter(!is.na(Depth))
  vals_block <- vals_block %>% filter(!is.na(Slope))
  if(nrow(vals_block) > 0){
    # Predict
    vals_block$Predicted <- predict(object=sub.model, data=vals_block, verbose=F)$predictions
    # predictions with cell IDs
    preds_block <- vals_block[c("cell","Predicted")]
    filename <- paste0("Predictions/Predictions_block", block, ".RData")
    save(preds_block, file = file.path(outRes, filename) )
    # clean up
    rm(vals_block, preds_block)
    gc(reset=T)
  }
}

# Apply
apply(iter, 1, predBlocks)



# Create empty template raster to add predictions to
r[] <- NA
names(r) <- 'substrate'
writeRaster(r, filename = "Substrate_template.tif", overwrite = TRUE)

# Try raster to avoid memory issues with terra
library(raster)
r <- raster("Substrate_template.tif")

# Add predictions to raster file by file to avoid memory issues
listblocks <- list.files(path='Results/Predictions', pattern='RData', full.names = T)
fill <- rep(NA, ncell(r))
for (b in 1:length(listblocks)){
  load(listblocks[b])
  fill[preds_block[['cell']]] <- as.numeric(preds_block[['Predicted']])
  print(b)
}
save(fill, file = file.path(outRes, 'All_Predictions.RData') )
#load(file.path(outRes, 'All_Predictions.RData') )

# Set values of raster
x <- raster::setValues(x=r, values=fill)

# Write raster
writeRaster(x, filename = "Substrate_HardSoft_Coastwide_10m_final.tif")








