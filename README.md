# 10m Coastwide Hard Substrate

__Authors:__      Jessica Nephin    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Source data](#source_data)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To predict hard and soft substrate coastwide at a 10m resolution.


## Summary
This readme describes the methods and source data used to derive a 10 m resolution coastwide hard substrate layer following on work from Gregr et al. (2021).


## Status
Complete


## Contents
This repository includes R code to predict hard and soft substrate from substrate observations (see Gregr et al. 2021) and a suite of predictor raster layers.


## Methods
See Gregr et al. 2021 for general methodology. The methods here differed in three main aspects:
1. Substrate observations were grouped into 2 categories: hard (rock, mixed) and soft (sand, mud) 
2. A different set of model predictors (covariates) were used because of the available source data (see #source_data)
3. In order to predict to the higher resolution coastwide, predictions were generated in blocks to avoid memory issues in R.


## Source data
The substrate observations were the same used in Gregr et al. 2021. The model predictors were: depth, slope, SD of depth, broad BPI, fine BPI, and current speed. Depth was sourced from a 10 m resolution coastwide bathymetry layer (Kung 2021). Slope, SD of depth, broad and fine BPI were derived from the bathymetry layer. The broad BPI range was 1 to 2.5 km and the fine BPI range was 30 m to 1 km. They were standardized prior to modelling. The current speed layer was derived from a number of regional oceanography models that were interpolated and mosaiced together to create a current speed index layer that covers the entire coast. For more details on methods and source data used to derive the current speed layer see https://gitlab.com/dfo-msea/environmental-layers/coastwide-current-index.


## Requirements
R version 4.2.1    
Terra 1.7-55 (R package)    
Raster 3.5-29 (R package)       


## Caveats
Different environmental raster layers were used in this version of the substrate model compared to the Gregr et al. 2021. Fetch was not available for the full study area so was not included as a model predictor.


## Acknowledgements
The r code created to generate the coastwide hard substrate predictions was based off code writen by Cole Fields and Ed Gregr.Bathymetry derivaties were generated using code based off scripts in the Benthic Terrain Modeler (BTM) 3.0 for ArcGIS authored by Dawn J. Wright, Emily R. Lundblad, Emily M. Larkin, Ronald W. Rinehart, Shaun Walbridge, Emily C. Huntley


## References
Gregr EJ, Haggarty DR, Davies SC, Fields C, Lessard J (2021) Comprehensive marine substrate classification applied to Canada's Pacific shelf. PLOS ONE 16(10): e0259156. https://doi.org/10.1371/journal.pone.0259156

Kung, Robert (2021) Canada west coast topo-bathymetric digital elevation model. Natural Resources Canada. Dataset. Accessed from https://open.canada.ca/data/en/dataset/e6e11b99-f0cc-44f7-f5eb-3b995fb1637e

